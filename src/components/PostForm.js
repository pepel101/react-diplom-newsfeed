import React, {useState} from "react";


function SendForm(props) {

    const [valueSelect, setValueSelect] = useState('')
    const [valueHeading, setValueHeading] = useState('')
    const [valuePostText, setValuePostText] = useState('')
    const selectOptions = props.state.map((item) => {
        return (<option>{item.name + ' ' + item.surname}</option>)
    })
    const handleChangeSelect = e => {
        setValueSelect(e.target.value)
    }
    const handleChangeHeading = e => {
        setValueHeading(e.target.value)
    }
    const handleChangePostText = e => {
        setValuePostText(e.target.value)
    }
    const handleSubmit = e => {
        e.preventDefault()
        props.onSubmit({
            id: Math.floor(Math.random() * 1000),
            author: valueSelect,
            heading: valueHeading,
            text: valuePostText,
            postDate: new Date().toLocaleTimeString()
        })
        setValueSelect('')
        setValueHeading('')
        setValuePostText('')
    }

    return (
        <form className="postForm" onSubmit={handleSubmit}>
            <h2>MY NEWS.</h2>
            <select onChange={handleChangeSelect} name="select" id="select" value={valueSelect}>
                <option>Change autor</option>
                {selectOptions}
            </select>
            <input value={valueHeading} onChange={handleChangeHeading} type="text" className="headingPost"
                   name="headingPost" placeholder="Your title..." maxLength="40"/>
            <textarea value={valuePostText} onChange={handleChangePostText} className="postText" name="postText"
                      placeholder="Your news..." rows="5"/>
            <button type="submit" className="sendPost">Submit</button>
        </form>
    )
}

export default SendForm