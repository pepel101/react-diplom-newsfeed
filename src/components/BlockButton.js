export function BlockButton({
                                sortAuthor, sortAuthor2,
                                sortHeading, sortHeading2,
                                sortText, sortText2,
                                sortDate, sortDate2
                            }) {
    return (
        <div className="blockButton">
            <h3>Sort by:</h3>
            <div>
                <span>Autor </span>
                <button onClick={sortAuthor} type="button" className="circleRed">+</button>
                <button onClick={sortAuthor2} type="button" className="circleGreen">-</button>
            </div>
            <div>
                <span>Title </span>
                <button onClick={sortHeading} type="button" className="circleRed">+</button>
                <button onClick={sortHeading2} type="button" className="circleGreen">-</button>
            </div>
            <div>
                <span>Text </span>
                <button onClick={sortText} type="button" className="circleRed">+</button>
                <button onClick={sortText2} type="button" className="circleGreen">-</button>
            </div>
            <div>
                <span>Date </span>
                <button onClick={sortDate} type="button" className="circleRed">+</button>
                <button onClick={sortDate2} type="button" className="circleGreen">-</button>
            </div>
        </div>
    )
}