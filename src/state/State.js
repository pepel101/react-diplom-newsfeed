import Sasha from '../images/Sasha.jpg'
import Boris from '../images/Boris.jpg'
import Vitold from '../images/Vitold.jpg'

export const state = [

    {
        "id": "1",
        "name": "User1",
        "surname":"surnameUser1",
        "age": 20,
        "city": "Минск",
        "street": "baker street",
        "house": 101,
        "email": "google@mail.ru",
        "education": "среднее"
        
    },
    {
        "id": "2",
        "name": "User2",
        "surname": "surnameUser2",
        "age": 25,
        "city": "Полоцк",
        "street": "zblBitskay street",
        "house": 102,
        "email": "tutby@mail.ru",
        "education": "высшее"
     
    },
    {
        "id": "3",
        "name": "User3",
        "surname": "surnameUser3",
        "age": 26,
        "city": "Брест",
        "street": "Oktyaborskay street",
        "house": 103,
        "email": "list@mail.ru",
        "education": "высшее"
    }
  
]

